# **README**

The thesis template is an [XMLmind ebook](http://xmlmind.com/xmleditor/_distrib/doc/ebook/title_page.html) document designed to assist students in preparing Bachelor, Master, and PhD theses. The XMLmind ebook format allows writing the content of a book in the fom of (X)HTML files, and define how they are bound together in an XML master file. 

You can use the [XMLmind XML Editor](http://xmlmind.com/xmleditor/), or the XMLmind XHTML Editor (xhe-perso-8_2_1-setup.exe) included in this repo, for writing the ebook XML file and the XHTML content files, and for generating HTML, PDF and ePub output.

For further instructions on the XMLmind ebook format (and their compiler), see the [XMLmind Ebook Compiler Manual](http://xmlmind.com/ebookc/_distrib/doc/manual/html1/manual.html).

## **Editing Instructions**

The thesis template can be edited using any text editor, but we recommend the use of an XML/HTML editor to help in identifying possible mistakes and speed the editing process. The recommended editors are:

1. [XMLmind XML Editor](http://xmlmind.com/xmleditor/) Attention: the free Personal Edition License does not allow to create correct HTML/PDF documents. Therefore, if you don't want to buy the Professional Edition User License (for 350 Euro), you can use the XMLmind XHTML Editor included in this repo in the file <kbd>xhe-perso-8_2_1-setup.exe</kbd>.
2. [Visual Studio Code](https://code.visualstudio.com)
3. [Notepad++](https://notepad-plus-plus.org/)

## **Conversion Instructions**

There are several methods for converting an XMLmind ebook to HTML, PDF or ePub.

### **Visual Studio Code**

The [Visual Studio Code](https://code.visualstudio.com/) provides several extensions that makes editing XML and HTML documents easy, see [HTML in Visual Studio Code](https://code.visualstudio.com/Docs/languages/html) for further details.

The conversion process can also be automated by integrating the [XMLmind Ebook Compiler](http://xmlmind.com/ebookc/) tool via tasks, see general instructions [here](https://code.visualstudio.com/Docs/editor/tasks).

To integrate the [XMLmind Ebook Compiler](http://xmlmind.com/ebookc/) with [Visual Studio Code](https://code.visualstudio.com/) on **Linux**

1. Download the [XMLmind Ebook Compiler](http://xmlmind.com/ebookc/download.shtml)
2. Install the [XMLmind Ebook Compiler](http://xmlmind.com/ebookc/) in a directory, referred in the next steps as `$EBOOK_HOME`
3. Create a directory named `.vscode` under the thesis template project directory
4. Create under the `.vscode` directory a file named `tasks.json` with the following content
    ```json
    {
      "version": "2.0.0",
      "tasks": [
        {
          "label": "ebookc pdf",
          "type": "shell",
          "linux": {
            "command": "$EBOOK_HOME/bin/ebookc -fop $EBOOK_HOME/plus/fop-2.3/fop/fop -p footer-separator no -p footer-left-width '6' -p footer-left '' -p footer-center-width '2' -p footer-center '' -p footer-right-width '2' -p footer-right '' -p header-separator no -p header-left-width '6' -p header-left 'one-page:: {{chapter-title}}' -p header-center-width '2' -p header-center '' -p header-right-width '2' -p header-right 'one-side:: {{page-number}}' -p paper-type 'A4' -p pdf-outline yes -p two-sided no -p show-external-links no ${file} ${workspaceFolder}/out/${fileBasenameNoExtension}.pdf"
          },
          "group": {
            "kind": "build",
            "isDefault": true
          }
        },
        {
          "label": "ebookc html",
          "type": "shell",
          "linux": {
            "command": "$EBOOK_HOME/bin/ebookc ${file} ${workspaceFolder}/out/${fileBasenameNoExtension}.html"
          },
          "group": {
            "kind": "build",
            "isDefault": true
          }
        }
      ]
    }
    ```
5. Open the `thesis.xml` file
6. Press `Ctrl`+`Shift`+`B` and select one of the `ebook pdf` or `ebook html` option to convert to PDF or HTML respectively
7. The output is placed under the `out` directory under the thesis template project directory

### **Notepad++**

[Notepad++](https://notepad-plus-plus.org/) can be used to edit the XML and HTML documents of the thesis template. To convert to PDF or HTML on **Windows**

1. Download the [XMLmind Ebook Compiler](http://xmlmind.com/ebookc/download.shtml)
2. Install the [XMLmind Ebook Compiler](http://xmlmind.com/ebookc/) in a directory, referred in the next steps as `$EBOOK_HOME`
3. To convert to **PDF**
  3.1. Open a command prompt
  3.2. Change directory to where the thesis template XML file resides
  3.3. Execute the command
      ```bash
      $EBOOK_HOME\bin\ebookc -fop $EBOOK_HOME\plus\fop-2.3\fop\fop -p footer-separator no -p footer-left-width '6' -p footer-left '' -p footer-center-width '2' -p footer-center '' -p footer-right-width '2' -p footer-right '' -p header-separator no -p header-left-width '6' -p header-left 'one-page:: {{chapter-title}}' -p header-center-width '2' -p header-center '' -p header-right-width '2' -p header-right 'one-side:: {{page-number}}' -p paper-type 'A4' -p pdf-outline yes -p two-sided no -p show-external-links no thesis.xml thesis.pdf
      ```
4. To convert to **HTML**
  4.1. Open a terminal window
  4.2. Change directory to where the thesis template XML file resides
  4.3. Execute the command
      ```bash
      $EBOOK_HOME\bin\ebookc thesis.xml thesis.html
      ```